import com.google.gson.JsonObject;
import com.squareup.okhttp.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class Main {
    public static String JBoxURL = "https://juicebox.hr/dnevni-meni/";
    public static String SlackURL = "https://hooks.slack.com/services/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";

    public static void main(String[] args) throws IOException {

        Document doc = null;
        try {
            doc = Jsoup.connect(JBoxURL)
                    .header("Cache-control", "no-cache")
                    .header("Cache-store", "no-store")
                    .maxBodySize(0)
                    .userAgent("Mozilla")
                    .get();


            Elements container = doc.getElementsByClass("vc-container");
            Element element = doc.getElementsByClass("vc-row-container container").get(2);
            Elements tBody = element.select("tbody");
            Elements tBody2 = element.select("p");

            //String title = container.select("h3").get(0).text();
            String title = "Dnevni meni | JUICE BOX";
            String subtitle = container.select("h1").get(0).text();

            String menuHtml = (tBody.text().trim() + tBody2.text().trim());
            String[] menu2 = menuHtml.split("(?<=kn)");
            String menuToExprt;
            String menu = "";

            for (int i = 0; i < menu2.length; i++) {
                menu = menu + (menu2[i] + "\n");
            }

            menuToExprt = title + "\n" + subtitle + "\n\n" + menu;
            System.out.println(menuToExprt);

            sendToSlack(menuToExprt);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void sendToSlack(String menu) {
        OkHttpClient client = new OkHttpClient();

        JsonObject object = new JsonObject();
        object.addProperty("text", menu);

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, object.toString());

        Request request = new Request.Builder()
                .url(SlackURL)
                .post(body)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                System.out.println("Failed: " + e.getMessage());
                System.exit(0);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                System.out.println("SUCCESS: " + response.body().string());
                System.exit(0);
            }
        });
    }
}
